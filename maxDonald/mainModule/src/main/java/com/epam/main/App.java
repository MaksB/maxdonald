package com.epam.main;


import com.epam.command.ComandMainExecutor;

public class App 
{
    public static void main( String[] args ) 
    {
    	ComandMainExecutor comandMainExecutor = new ComandMainExecutor();
    	comandMainExecutor.run();
    }
}
