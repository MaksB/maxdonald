package com.epam.command;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.epam.scanner.ScannerInstance;

import java.util.Scanner;

public class ComandMainExecutor {

	private static final String ORDER = "order";
	private static final String INCORECT_DATA = "Incorect data";
	private static final String MENU = "Menu:";
	private static final String EXIT = "exit";
	private static final String ENTER_COMMAND = "Enter command:";
	private HashMap<String, Command> mapCommadn;

	public ComandMainExecutor() {
		mapCommadn = new LinkedHashMap<>();
		init();
	}
	
	private void init(){
		mapCommadn.put(EXIT, new ExitCommand());
		mapCommadn.put(ORDER, new ShowOrderTypeMenuCommand());
	}

	public void run() {
		System.out.println(MENU);
		showCommand();
		Scanner scanner = ScannerInstance.getInstance();
		for (;;) {
			System.out.println(ENTER_COMMAND);
			String key = scanner.nextLine();

			if (mapCommadn.containsKey(key)) {
				mapCommadn.get(key).execute();
			}else {
				System.out.println(INCORECT_DATA);
				run();
			}

		}
	}
	
	private void showCommand(){
		for (Entry<String, Command> entry: mapCommadn.entrySet()) {
			System.out.println(entry.getKey());
		}
	}
}
