package com.epam.command;

public interface Command {

	public void execute();
	public String getName();
}
