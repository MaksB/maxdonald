package com.epam.command;

import java.util.Scanner;

import com.epam.model.order.Order;
import com.epam.model.order.ResultOrder;
import com.epam.scanner.ScannerInstance;
import com.epam.service.OrderService;

public class BuyCommand implements Command {

	private static final String YOUR_ORDER_CANCELED = "Your order canceled";
	private static final String INCORRECT_DATA = "Incorrect data";
	private static final String BUY = "buy";
	private static final String CANCEL = "cancel";
	private static final String CONSUME = "consume";
	private static final String ENTER_COMMAND = "Enter command:";
	private OrderService orderService;
	private Order order;

	public BuyCommand(OrderService orderService, Order order) {
		this.order = order;
		this.orderService = orderService;
	}

	@Override
	public void execute() {
		ResultOrder resultOrder = orderService.getOrder(order);
		System.out.println("Your order:" + resultOrder);
		Scanner scanner = ScannerInstance.getInstance();
		for (;;) {
			showCommand();
			System.out.println(ENTER_COMMAND);
			String key = scanner.nextLine();
			if(key.contains(CONSUME)){
				System.out.println("Bon appetit");
				System.exit(0);
			}else if(key.contains(CANCEL)){
				orderService.cancelOrder(resultOrder);
				System.out.println(YOUR_ORDER_CANCELED);
				new ComandMainExecutor().run();
			}else{
				System.out.println(INCORRECT_DATA);
			}
		}
	}

	private void showCommand() {
		System.out.println(CONSUME + "\n"+CANCEL);
	}

	@Override
	public String getName() {
		return BUY;
	}

	@Override
	public String toString() {
		return "BuyCommand [orderService=" + orderService + ", order=" + order + "]";
	}
	
	
}
