package com.epam.command;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Map.Entry;

import com.epam.abstractfactory.AbstractFactoryService;
import com.epam.abstractfactory.AbstractFactoryServiceImpl;
import com.epam.model.order.Order;
import com.epam.modelenum.DrinksMenu;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.OrderType;
import com.epam.scanner.ScannerInstance;
import com.epam.service.OrderService;

public class DrinkOrderTypeCommand implements Command {
	private static final String ADD_ITEMS = "Add items then buy";
	private static final String MESSAGE = "Incorrect data";
	private static final String ADD_ITEM = "Add item:__";
	private static final String DRINK = "drink";
	private static final String BUY = "buy";
	private static final String EXIT = "exit";
	private HashMap<String, Command> mapCommadn;
	private OrderService orderService;

	public DrinkOrderTypeCommand() {
		mapCommadn = new LinkedHashMap<>();
		init();
	}

	private void init() {
		AbstractFactoryService abstractFactoryService = new AbstractFactoryServiceImpl();
		orderService = abstractFactoryService.getDrinkService().getOrderService();
		mapCommadn.put(EXIT, new ExitCommand());
	}

	@Override
	public void execute() {
		showCommand();
		Scanner scanner = ScannerInstance.getInstance();

		Order order = new Order();
		order.setOrderType(OrderType.DRINKS);
		mapCommadn.put(BUY, new BuyCommand(orderService, order));
		for (;;) {

			String key = scanner.nextLine();

			if (mapCommadn.containsKey(key)) {
				if (key.contains("buy") && order.getFoodList().isEmpty()) {
					System.out.println(ADD_ITEMS);
					continue;

				}
				mapCommadn.get(key).execute();
			} else {
				try {
					order.getFoodList().add(DrinksMenu.valueOf(key));
				} catch (Exception e) {
					System.out.println(MESSAGE);
				}
			}
			showCommand();
			System.out.println("Your order:" + order);
		}

	}

	private void showCommand() {
		System.out.println(ADD_ITEM);
		for (FoodMenu foodMenu : OrderType.DRINKS.getFood()) {
			System.out.println(foodMenu);
		}
		for (Entry<String, Command> entry : mapCommadn.entrySet()) {
			System.out.println(entry.getKey());
		}
	}

	@Override
	public String getName() {
		return DRINK;
	}

}
