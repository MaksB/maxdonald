package com.epam.command;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

import com.epam.abstractfactory.AbstractFactoryService;
import com.epam.abstractfactory.AbstractFactoryServiceImpl;
import com.epam.model.order.Order;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.OrderType;
import com.epam.modelenum.VegetarianMenu;
import com.epam.scanner.ScannerInstance;
import com.epam.service.OrderService;

import java.util.Map.Entry;

public class VegetarianOrderTypeCommand implements Command {

	private static final String BUY = "buy";
	private static final String SUBJECT_MESSAGE = "Add item:__";
	private static final String MESSAGE = "Add items then buy";
	private static final String EXCEPTION_MESSAGE = "Incorrect data";
	private static final String VEGETARIAN = "vegetarian";
	private static final String EXIT = "exit";
	private HashMap<String, Command> mapCommadn;
	private OrderService orderService;

	public VegetarianOrderTypeCommand() {
		mapCommadn = new LinkedHashMap<>();
		init();
	}

	private void init() {
		AbstractFactoryService abstractFactoryService = new AbstractFactoryServiceImpl();
		orderService = abstractFactoryService.getVegetarianService().getOrderService();
		mapCommadn.put(EXIT, new ExitCommand());
	}

	@Override
	public void execute() {

		showCommand();
		Scanner scanner = ScannerInstance.getInstance();
		
		Order order = new Order();
		order.setOrderType(OrderType.VEGETARIAN);
		mapCommadn.put(BUY, new BuyCommand(orderService, order));
		for (;;) {

			String key = scanner.nextLine();

			if (mapCommadn.containsKey(key)) {
				if (key.contains(BUY) && order.getFoodList().isEmpty()) {
						System.out.println(MESSAGE);
						continue;
					
				}
				mapCommadn.get(key).execute();
			} else {
				try {
					order.getFoodList().add(VegetarianMenu.valueOf(key));
				} catch (Exception e) {
					System.out.println(EXCEPTION_MESSAGE);
				}
			}
			showCommand();
			System.out.println("Your order:" + order);
		}

	}

	private void showCommand() {
		System.out.println(SUBJECT_MESSAGE);
		for (FoodMenu foodMenu : OrderType.VEGETARIAN.getFood()) {
			System.out.println(foodMenu);
		}
		for (Entry<String, Command> entry : mapCommadn.entrySet()) {
			System.out.println(entry.getKey());
		}
	}

	@Override
	public String getName() {
		return VEGETARIAN;
	}
}
