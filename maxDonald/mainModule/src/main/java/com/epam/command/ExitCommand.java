package com.epam.command;

public class ExitCommand implements Command{

	private static final String EXIT = "exit";
	@Override
	public void execute() {
		System.exit(0);
	}
	@Override
	public String getName() {
		return EXIT;
	}
}
