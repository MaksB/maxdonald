package com.epam.command;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Map.Entry;

import com.epam.scanner.ScannerInstance;

public class ShowOrderTypeMenuCommand implements Command {

	private static final String ORDER = "order";
	private static final String INCORRECT_DATA = "Incorrect data";
	private static final String ENTER_COMMAND = "Enter command";
	private static final String ORDER_TYPE = "Order type:__";
	private HashMap<String, Command> mapCommadn;

	public ShowOrderTypeMenuCommand() {
		mapCommadn = new LinkedHashMap<>();
		init();
	}

	private void init() {
		StandratOrderTypeCommand standratOrderTypeCommand = new StandratOrderTypeCommand();
		VegetarianOrderTypeCommand vegetarianOrderTypeCommand = new VegetarianOrderTypeCommand();
		DrinkOrderTypeCommand drinkOrderTypeCommand = new DrinkOrderTypeCommand();
		ExitCommand exitCommand = new ExitCommand();
		mapCommadn.put(standratOrderTypeCommand.getName(), standratOrderTypeCommand);
		mapCommadn.put(vegetarianOrderTypeCommand.getName(), vegetarianOrderTypeCommand);
		mapCommadn.put(drinkOrderTypeCommand.getName(), drinkOrderTypeCommand);
		mapCommadn.put(exitCommand.getName(), exitCommand);
	}

	@Override
	public void execute() {
		System.out.println(ORDER_TYPE);
		showCommand();
		Scanner scanner = ScannerInstance.getInstance();
		for (;;) {
			System.out.println(ENTER_COMMAND);
			String key = scanner.nextLine();

			if (mapCommadn.containsKey(key)) {
				mapCommadn.get(key).execute();
			} else {
				System.out.println(INCORRECT_DATA);
				execute();
			}

		}
	}

	private void showCommand() {
		for (Entry<String, Command> entry : mapCommadn.entrySet()) {
			System.out.println(entry.getKey());
		}
	}

	@Override
	public String getName() {
		return ORDER;
	}
}
