package com.epam.scanner;

import java.util.Scanner;

public class ScannerInstance {
	private ScannerInstance() {
	}

	public static Scanner getInstance() {
		return ScannerSinglton.INSTANCE.getInstance();
	}

	private enum ScannerSinglton {
		INSTANCE;
		private Scanner scanner;

		public Scanner getInstance() {
			if (scanner == null) {
				scanner = new Scanner(System.in);
			}
			return scanner;
		}

	}

}
