package com.epam.kitchen;

import java.util.HashMap;
import java.util.Map;

import com.epam.kitchen.exception.IncorectMenuTypeException;
import com.epam.model.eat.Bigmuck;
import com.epam.model.eat.Bun;
import com.epam.model.eat.Burger;
import com.epam.model.eat.Chees;
import com.epam.model.eat.Food;
import com.epam.model.eat.IceCream;
import com.epam.model.eat.Salad;
import com.epam.model.eat.Soy;
import com.epam.model.eat.applications.Ketchup;
import com.epam.model.eat.applications.Salt;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.VegetarianMenu;

public class FactroryCookingVegMenu implements FactoryKitchen {

	private static Map<FoodMenu, Food> clonePool = new HashMap<>();

	static {
		clonePool.put(VegetarianMenu.VEGA_BURGER, new Burger.BurgerBuilder(new Bun())
				.addKetchup(new Ketchup()).addSoy(new Soy()).addSalad(new Salad()).addSalt(new Salt()).crate());
		clonePool.put(VegetarianMenu.VEGA_BIGMUCK,
				new Bigmuck.BigmuckBilder(new Bun()).addChees(new Chees()).addFirstSoy(new Soy())
						.addSecondSoy(new Soy()).addSalad(new Salad()).addKectchup(new Ketchup()).create());
	}

	@Override
	public Food cooking(FoodMenu foodMenu) {
		Food eat;
		VegetarianMenu vegetarianMenu = (VegetarianMenu) foodMenu;
		switch (vegetarianMenu) {
		case VEGA_BURGER:
			eat = (Food) clonePool.get(foodMenu).cloneFood();
			break;
		case VEGA_BIGMUCK:
			eat = (Food) clonePool.get(foodMenu).cloneFood();
			break;
		case ICE_CREAM:
			eat = new IceCream();
			break;
		default:
			throw new IncorectMenuTypeException();
		}
		return eat;
	}
}
