package com.epam.kitchen;

import java.util.HashMap;
import java.util.Map;

import com.epam.kitchen.exception.IncorectMenuTypeException;
import com.epam.model.eat.Bigmuck;
import com.epam.model.eat.Bun;
import com.epam.model.eat.Burger;
import com.epam.model.eat.Chees;
import com.epam.model.eat.Food;
import com.epam.model.eat.IceCream;
import com.epam.model.eat.Meat;
import com.epam.model.eat.Salad;
import com.epam.model.eat.applications.Ketchup;
import com.epam.model.eat.applications.Salt;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.StandartMenu;

public class FactoryCookingEat implements FactoryKitchen {

	private static Map<FoodMenu, Food> clonePool = new HashMap<>();

	static {
		clonePool.put(StandartMenu.BURGER, new Burger.BurgerBuilder(new Bun()).addKetchup(new Ketchup())
				.addMeat(new Meat()).addSalad(new Salad()).addSalt(new Salt()).crate());
		clonePool.put(StandartMenu.BIGMUCK,
				new Bigmuck.BigmuckBilder(new Bun()).addChees(new Chees()).addFirstMeat(new Meat())
						.addSecondMeat(new Meat()).addSalad(new Salad()).addKectchup(new Ketchup()).create());
	}

	@Override
	public Food cooking(FoodMenu foodMenu) {
		Food food;
		StandartMenu standartMenu;
		standartMenu = (StandartMenu) foodMenu;
		switch (standartMenu) {
		case BURGER:
			food = (Food) clonePool.get(foodMenu).cloneFood();
			break;
		case BIGMUCK:
			food = (Food) clonePool.get(foodMenu).cloneFood();
			break;
		case ICE_CREAM:
			food = new IceCream();
			break;
		default:
			throw new IncorectMenuTypeException();
		}

		return food;
	}
}
