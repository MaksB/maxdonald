package com.epam.modelenum;

import java.util.Arrays;
import java.util.List;

public enum OrderType {
	DRINKS(DrinksMenu.class), STANDARD(StandartMenu.class), VEGETARIAN(VegetarianMenu.class);

	private Class<? extends FoodMenu> foodMenu;

	private OrderType(Class<? extends FoodMenu> foodMenu) {
		this.foodMenu = foodMenu;
	}
	
	public List<FoodMenu> getFood(){
		return Arrays.asList(foodMenu.getEnumConstants());
	}

}
