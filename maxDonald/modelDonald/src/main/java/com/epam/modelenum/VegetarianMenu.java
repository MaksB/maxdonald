package com.epam.modelenum;

public enum VegetarianMenu implements FoodMenu {
	VEGA_BURGER, VEGA_BIGMUCK, ICE_CREAM;
}
