package com.epam.model.drink;

import com.epam.model.eat.Food;
import com.epam.model.eat.PrototypeFood;

public class Water extends Food {
	private boolean gas;

	public Water() {
		COST = 0;
		name = "Organic";
		gas = false;
	}
	
	public boolean isGas() {
		return gas;
	}

	public void setGas(boolean gas) {
		this.gas = gas;
	}

	@Override
	public PrototypeFood cloneFood() {
		Water water = new Water();
		water.setCost(this.COST);
		water.setGas(this.gas);
		water.setName(this.name);
		return water;
	}

	@Override
	public String toString() {
		return "Water [gas=" + gas + ", cost=" + COST + ", name=" + name + "]";
	}
	
	

}
