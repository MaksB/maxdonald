package com.epam.model.eat;

public abstract class Food implements PrototypeFood {

	protected double COST = 0.0;
	protected String name;

	public double getCost() {
		return COST;
	}

	public void setCost(double cost) {
		this.COST = cost;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
