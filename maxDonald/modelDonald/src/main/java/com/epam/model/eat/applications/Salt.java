package com.epam.model.eat.applications;

import com.epam.model.eat.Food;
import com.epam.model.eat.PrototypeFood;

public class Salt extends Food{

	public Salt() {
		name = "Salt";
		COST = 0;
	}
	
	@Override
	public PrototypeFood cloneFood() {
		Salt salt = new Salt();
		salt.setCost(COST);
		salt.setName(name);
		return salt;
	}

	@Override
	public String toString() {
		return "Salt [cost=" + COST + ", name=" + name + "]";
	}
	
	
}
