package com.epam.model.order;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.OrderType;

public class Order {

	private List<FoodMenu> foodMenuList;
	private OrderType orderType;
	private Date date;
	
	public Order() {
		foodMenuList = new LinkedList<>();
		date = new Date();
	}
	
	public List<FoodMenu> getFoodList() {
		return foodMenuList;
	}
	public void setFoodList(List<FoodMenu> foodList) {
		this.foodMenuList = foodList;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Order [foodMenuList=" + foodMenuList + ", orderType=" + orderType + ", date=" + date + "]";
	}


	
	
	
}
