package com.epam.model.eat;

public class Chees extends Food {
	private String cauntry;

	public Chees() {
		cauntry = "Italian";
		COST = 10;
		name = "Parmezan";
	}

	public String getCauntry() {
		return cauntry;
	}

	public void setCauntry(String cauntry) {
		this.cauntry = cauntry;
	}

	@Override
	public PrototypeFood cloneFood() {
		Chees chees = new Chees();
		chees.setCauntry(cauntry);
		chees.setName(name);
		chees.setCost(COST);
		return chees;
	}

	@Override
	public String toString() {
		return "Chees [cauntry=" + cauntry + ", cost=" + COST + ", name=" + name + "]";
	}

	
}
