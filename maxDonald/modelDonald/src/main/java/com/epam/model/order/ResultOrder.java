package com.epam.model.order;

import java.util.ArrayList;
import java.util.List;

import com.epam.model.eat.Food;

public class ResultOrder {

	private Order order;
	
	private List<Food> foodList;
	private double price;
	
	public ResultOrder(Order order) {
		this.order = order;
		foodList = new ArrayList<>();
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public List<Food> getFoodList() {
		return foodList;
	}

	public void setFoodList(List<Food> foodList) {
		this.foodList = foodList;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ResultOrder [price=" + price +" "+ order + ", foodList=" + foodList + "]";
	}
	
	
	
	
}
