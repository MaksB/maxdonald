package com.epam.model.drink;

import com.epam.model.eat.Food;
import com.epam.model.eat.PrototypeFood;

public class Soda extends Food{
	private int sugar;

	public Soda() {
		sugar = 20;
		COST = 5;
		name = "Fanta";
	}
	
	public int getSugar() {
		return sugar;
	}

	public void setSugar(int sugar) {
		this.sugar = sugar;
	}
	
	@Override
	public PrototypeFood cloneFood() {
		Soda soda = new Soda();
		soda.setCost(this.COST);
		soda.setName(this.name);
		soda.setSugar(this.sugar);
		return soda;
	}

	@Override
	public String toString() {
		return "Soda [sugar=" + sugar + ", cost=" + COST + ", name=" + name + "]";
	}
	
	
	
}
