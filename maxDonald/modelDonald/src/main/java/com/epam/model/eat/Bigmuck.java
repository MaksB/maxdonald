package com.epam.model.eat;

import com.epam.model.eat.applications.Ketchup;
import com.epam.model.eat.applications.Mustard;
import com.epam.model.eat.applications.Salt;

public class Bigmuck extends Food {

	private Bun bun;
	private Chees chees;
	private Meat firstMeat;
	private Meat secondMeat;
	private Soy firstSoy;
	private Soy secondSoy;
	private Ketchup ketchup;
	private Mustard mustard;
	private Salad salad;
	private Salt salt;

	private Bigmuck() {
	}

	public Bigmuck(BigmuckBilder bigmuckBilder) {
		this.bun = bigmuckBilder.bun;
		this.chees = bigmuckBilder.chees;
		this.firstMeat = bigmuckBilder.firstMeat;
		this.secondMeat = bigmuckBilder.secondMeat;
		this.firstSoy = bigmuckBilder.firstSoy;
		this.secondSoy = bigmuckBilder.secondSoy;
		this.ketchup = bigmuckBilder.ketchup;
		this.mustard = bigmuckBilder.mustard;
		this.salad = bigmuckBilder.salad;
		this.salt = bigmuckBilder.salt;
		this.name = "Burger";
		сalculateCost();
	}

	public static class BigmuckBilder {
		private final Bun bun;
		private Chees chees;
		private Meat firstMeat;
		private Meat secondMeat;
		private Soy firstSoy;
		private Soy secondSoy;
		private Ketchup ketchup;
		private Mustard mustard;
		private Salad salad;
		private Salt salt;

		public BigmuckBilder(Bun bun) {
			this.bun = bun;
		}

		public BigmuckBilder addChees(Chees chees) {
			this.chees = chees;
			return this;
		}

		public BigmuckBilder addFirstMeat(Meat meat) {
			this.firstMeat = meat;
			return this;
		}

		public BigmuckBilder addSecondMeat(Meat secondMeat) {
			this.secondMeat = secondMeat;
			return this;
		}

		public BigmuckBilder addFirstSoy(Soy firstSoy) {
			this.firstSoy = firstSoy;
			return this;
		}

		public BigmuckBilder addSecondSoy(Soy secondSoy) {
			this.secondSoy = secondSoy;
			return this;
		}

		public BigmuckBilder addKectchup(Ketchup ketchup) {
			this.ketchup = ketchup;
			return this;
		}

		public BigmuckBilder addMustard(Mustard mustard) {
			this.mustard = mustard;
			return this;
		}

		public BigmuckBilder addSalad(Salad salad) {
			this.salad = salad;
			return this;
		}

		public BigmuckBilder addSalt(Salt salt) {
			this.salt = salt;
			return this;
		}

		public Bigmuck create() {
			return new Bigmuck(this);
		}
	}

	public Bun getBun() {
		return bun;
	}

	public void setBun(Bun bun) {
		this.bun = bun;
	}

	public Chees getChees() {
		return chees;
	}

	public void setChees(Chees chees) {
		this.chees = chees;
	}

	public Meat getFirstMeat() {
		return firstMeat;
	}

	public void setFirstMeat(Meat firstMeat) {
		this.firstMeat = firstMeat;
	}

	public Meat getSecondMeat() {
		return secondMeat;
	}

	public void setSecondMeat(Meat secondMeat) {
		this.secondMeat = secondMeat;
	}

	public Soy getFirstSoy() {
		return firstSoy;
	}

	public void setFirstSoy(Soy firstSoy) {
		this.firstSoy = firstSoy;
	}

	public Soy getSecondSoy() {
		return secondSoy;
	}

	public void setSecondSoy(Soy secondSoy) {
		this.secondSoy = secondSoy;
	}

	public Ketchup getKetchup() {
		return ketchup;
	}

	public void setKetchup(Ketchup ketchup) {
		this.ketchup = ketchup;
	}

	public Mustard getMustard() {
		return mustard;
	}

	public void setMustard(Mustard mustard) {
		this.mustard = mustard;
	}

	public Salad getSalad() {
		return salad;
	}

	public void setSalad(Salad salad) {
		this.salad = salad;
	}

	public Salt getSalt() {
		return salt;
	}

	public void setSalt(Salt salt) {
		this.salt = salt;
	}
	
	private void сalculateCost(){
		COST += bun.getCost();
		if (chees != null) {
			COST +=chees.getCost();
		}
		if (firstMeat != null) {
			COST += firstMeat.getCost();
		}
		if (secondMeat != null) {
			COST += secondMeat.getCost();
		}
		if (firstSoy != null) {
			COST += firstSoy.getCost();
		}
		if (secondSoy != null) {
			COST += secondSoy.getCost();
		}
		if (ketchup != null) {
			COST += ketchup.getCost();
		}
		if (mustard != null) {
			COST += mustard.getCost();
		}
		if (salad != null) {
			COST += salad.getCost();
		}
		if (salt != null) {
			COST += salt.getCost();
		}
	}

	@Override
	public PrototypeFood cloneFood() {
		Bigmuck bigmuck = new Bigmuck();
		bigmuck.setBun(bun);
		bigmuck.setCost(COST);
		bigmuck.setName(name);
		if (chees != null) {
			bigmuck.setChees(chees);
		}
		if (firstMeat != null) {
			bigmuck.setFirstMeat(firstMeat);
		}
		if (secondMeat != null) {
			bigmuck.setSecondMeat(secondMeat);
		}
		if (firstSoy != null) {
			bigmuck.setFirstSoy(firstSoy);
		}
		if (secondSoy != null) {
			bigmuck.setSecondSoy(secondSoy);
		}
		if (ketchup != null) {
			bigmuck.setKetchup(ketchup);
		}
		if (mustard != null) {
			bigmuck.setMustard(mustard);
		}
		if (salad != null) {
			bigmuck.setSalad(salad);
		}
		if (salt != null) {
			bigmuck.setSalt(salt);
		}
		return bigmuck;
	}

	@Override
	public String toString() {
		return "Bigmuck [bun=" + bun + ", chees=" + chees + ", firstMeat=" + firstMeat + ", secondMeat=" + secondMeat
				+ ", firstSoy=" + firstSoy + ", secondSoy=" + secondSoy + ", ketchup=" + ketchup + ", mustard="
				+ mustard + ", salad=" + salad + ", salt=" + salt + ", cost=" + COST + ", name=" + name + "]";
	}
	

}
