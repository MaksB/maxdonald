package com.epam.service.standart;

import java.util.List;

import com.epam.kitchen.exception.IncorectMenuTypeException;
import com.epam.model.eat.Food;
import com.epam.model.order.Order;
import com.epam.model.order.ResultOrder;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.OrderType;
import com.epam.pool.eat.EatPool;
import com.epam.service.OrderService;

public class StandartOrderService implements OrderService {

	private EatPool eatPool;

	public StandartOrderService() {
		eatPool = EatPool.getInstance();
	}

	@Override
	public void cancelOrder(ResultOrder resultOrder) {
		for (int n = 0; n <= resultOrder.getOrder().getFoodList().size() - 1; n++) {
			eatPool.saveFood(resultOrder.getFoodList().get(n), resultOrder.getOrder().getOrderType(),
					resultOrder.getOrder().getFoodList().get(n));
		}

	}

	@Override
	public ResultOrder getOrder(Order order) {
		if (order.getOrderType() == OrderType.STANDARD) {
			ResultOrder resultOrder = new ResultOrder(order);
			for (FoodMenu foodMenu : order.getFoodList()) {
				Food food = eatPool.getEat(order.getOrderType(), foodMenu);
				resultOrder.getFoodList().add(food);
			}
			resultOrder.setPrice(calculatePrice(resultOrder.getFoodList()));
			return resultOrder;
		} else {
			throw new IncorectMenuTypeException();
		}
	}

	private double calculatePrice(List<Food> foods) {
		double cost = 0.0;
		for (Food food : foods) {
			cost += food.getCost();
		}
		return cost;
	}
}
