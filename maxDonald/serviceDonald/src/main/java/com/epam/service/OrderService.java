package com.epam.service;

import com.epam.model.order.Order;
import com.epam.model.order.ResultOrder;

public interface OrderService {

	public ResultOrder getOrder(Order order);
	public void cancelOrder(ResultOrder resultOrder);
}
