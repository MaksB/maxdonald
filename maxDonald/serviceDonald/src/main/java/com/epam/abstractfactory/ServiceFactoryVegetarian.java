package com.epam.abstractfactory;

import com.epam.service.OrderService;
import com.epam.service.vegetarian.VegetarianOrderService;

public class ServiceFactoryVegetarian implements ServiceFactory{

	@Override
	public OrderService getOrderService() {
		return new VegetarianOrderService();
	}

}
