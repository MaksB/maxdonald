package com.epam.abstractfactory;

import com.epam.service.OrderService;
import com.epam.service.drink.DrinkOrderService;

public class ServiceFactoryDrink implements ServiceFactory{

	@Override
	public OrderService getOrderService() {
		return new DrinkOrderService();
	}
}
