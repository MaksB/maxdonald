package com.epam.abstractfactory;

import com.epam.service.OrderService;

public interface ServiceFactory {
	public OrderService getOrderService();
}
