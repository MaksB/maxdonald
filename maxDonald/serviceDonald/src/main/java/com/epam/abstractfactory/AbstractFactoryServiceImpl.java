package com.epam.abstractfactory;

public class AbstractFactoryServiceImpl implements AbstractFactoryService{

	@Override
	public ServiceFactory getStandartService() {
		return  new ServiceFactroyStandart();
	}
	
	@Override
	public ServiceFactory getVegetarianService() {
		return new ServiceFactoryVegetarian();
	}
	
	@Override
	public ServiceFactory getDrinkService() {
		return new ServiceFactoryDrink();
	}
}
